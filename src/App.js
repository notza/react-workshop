import React from "react";
import PropTypes from "prop-types";
import CssBaseline from "@material-ui/core/CssBaseline";
import MenuBar from "./Component/header/index";
import Routes from "./router";

function App(props) {
  const { classes } = props;
  return (
    <React.Fragment>
      <CssBaseline />

      <Routes />
    </React.Fragment>
  );
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default App;
