import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import SignIn from "./Component/login/index";
import Register from "./Component/register/register";
import NotFound from "././Component/NotFound/NotFound";
import Welcome from "./Component/welcome/index";
import About from "./Component/page/about";
import MenuBar from "./Component/header/index";
import PrivateRoute from "./PrivateRoute";

const Routes = () => (
  <Router>
    <div>
      <MenuBar />
      <Route exact path="/" component={Welcome} />
      <Route path="/about" component={About} />
      <Route path="/login" component={SignIn} />
      <Route path="/register" component={Register} />
      <PrivateRoute path="/protected" component={Welcome} />
    </div>
  </Router>
);

export default Routes;
