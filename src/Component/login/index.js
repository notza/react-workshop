import React, { Component } from "react";
import PropTypes from "prop-types";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import { Field, reduxForm } from "redux-form";
import { SubmissionError } from "redux-form";
import { Alert } from "antd";
import { connect } from "react-redux";
import { PostData } from "../../Service/Api";
import Auth from "../../Auth/index";
import { Redirect } from "react-router-dom";

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: "",
      error: 0
    };
    this.onChange = this.onChange.bind(this);
  }

  handleSubmit = async event => {
    let data = {
      username: this.state.username,
      password: this.state.password
    };
    console.log(data);
  };

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  SignIn = () => {
    const { error, handleSubmit, classes } = this.props;

    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Login
          </Typography>
          <form className={classes.form} onSubmit={this.handleSubmit}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">Username</InputLabel>
              <Input
                name="username"
                type="text"
                ref={input => (this.username = input)}
                onChange={this.onChange}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">password</InputLabel>
              <Input
                name="password"
                type="password"
                ref={input => (this.password = input)}
                onChange={this.onChange}
              />
            </FormControl>

            {error && <strong>{error}</strong>}

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Submit
            </Button>
          </form>
        </Paper>
      </main>
    );
  };

  render() {
    return this.SignIn();
  }
}

const SubmitValidation = withStyles(styles)(Login);
const SubmitValidationForm = reduxForm({
  form: "Login-form" // a unique identifier for this form
})(SubmitValidation);
export default connect()(SubmitValidationForm);
