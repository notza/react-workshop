import React from "react";
import PropTypes, { func } from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { Link } from "react-router-dom";

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  Link: {
    color: "#000"
  }
};
const login = props => <Link to="/login" {...props} />;
const register = props => <Link to="/register" {...props} />;
const about = props => <Link to="/about" {...props} />;
function MenuBar(props) {
  const { classes } = props;
  console.log(props);
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            <Link to={"/"}>Diversition</Link>
          </Typography>
          <Button color="inherit" component={about}>
            About
          </Button>{" "}
          <Button color="inherit" component={login}>
            Login
          </Button>{" "}
          <Button color="inherit" component={register}>
            Register
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

MenuBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MenuBar);
